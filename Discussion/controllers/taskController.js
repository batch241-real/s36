// Controllers contains the function and business logic of our Express JS application
// meaning all the operations it can do will be placed in this file

// uses the 'require' directive to allow access to the 'Task' model which allows us to access methods to perform CRUD operations
// allows us to use the contents of the 'task.js' in the 'models' folder

const Task = require('../models/task');

// Controller function for GETTING ALL THE TASKS
// defines the functions to be used in the 'taskRoutes.js' file and export these functions
module.exports.geAllTasks = () => {
	
	// '.then' method used to wait for the Mongoose 'find' method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		return result;
	});
}

module.exports.createTask = (requestBody) => {
	
	// create a task object based on the Mongoose Model 'task'

	// sets the 'name' property with 
	let newTask = new Task({
		name: requestBody.name
	})

	// the first parameter will store the result return by the Mongoose save method
	// the second parameter will store the 'error' object
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			return task;
		}
	})
}


module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		} else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, reqBody) =>{
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.name = reqBody.name;

		return result.save().then((updatedTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updatedTask;
			}
		})
	})
}

module.exports.getATask = (taskId) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		} else{
			return result;
		}
	});
}


module.exports.updateTaskStatus = (taskId, reqBody) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = reqBody.status;

		return result.save().then((updatedStatus, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else{
				return updatedStatus;
			}
		})
	})
}