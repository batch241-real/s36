// contains all the endpoints for our application
// we seperate the routes such that 'index.js' only contains information on the server

const express = require('express');
const taskController = require('../controllers/taskController');

// creates a Router instance that functions as a middleware and routing system
// allow access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();

// Routes
// the routes are responsible for defining the URIs that our client accesses and the corresponding controller function that will be used when a route is accessed
// they invoke the controller functions from the controller files
// all the business logic is done in the controller

// Route to GET ALL THE TASK
router.get('/', (req, res) => {

	taskController.geAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route for CREATING A NEW TASK
router.post('/', (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})


// Route for DELETING A TASK
router.delete('/:id', (req, res) => {
	console.log(req.params);
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route for  UPDATING A TASK
router.put('/:id', (req,res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


// Route for GETTING A TASK
router.get('/:id', (req, res) => {
	taskController.getATask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route for UPDATING TASK STATUS
router.put('/:id/complete', (req,res) => {
	taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

// use 'module.exports' to export the router object to use in the 'index.js'
module.exports = router;